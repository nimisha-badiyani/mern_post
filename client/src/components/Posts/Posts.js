import React, { useEffect } from 'react';
import { Grid } from '@material-ui/core';
// import { useSelector } from 'react-redux';

import Post from './Post/Post';
import useStyles from './styles';

const Posts = ({ setCurrentId }) => {
  // const posts = useSelector((state) => state.posts);
  const [Posts, setPosts] = React.useState([]);
  useEffect(() => {
    async function getPosts() {
      const response = await fetch(`http://localhost:5000/posts`);
      console.log(response);
      const Posts = await response.json();
      // alert(Posts.message);
      setPosts(Posts);
    }
    return getPosts();
  }, []);
  const classes = useStyles();

  return (
      <Grid className={classes.container} container alignItems="stretch" spacing={3}>
        {Posts.map((post) => (
          <Grid key={post._id} item xs={12} sm={6} md={6}>
            <Post post={post} setCurrentId={setCurrentId} />
          </Grid>
        ))}
      </Grid>
    )
};

export default Posts;
